You may have to set your java.home setting in VSCode to get the java plugins to work.

If you notice a spinning circle in the bottom right corner of VSCode, click on it and if
it is complaining about starting a java server then this should sort it out:

File->Preferences->Settings

Type java.home in the search box and click the "Edit in settings.json" link in the window

Add the config item: "java.home": "C:\\Program Files\\RedHat\\java-11-openjdk-11.0.8-2",

(Check the path above is valid on your machine)


------------------------------------------------------------------------------------------


To create an empty gradle project open a powershell prompt (e.g. inside VScode) and do:

Create an empty directory and make sure that's your working directory.

PS C:\Users\Administrator\Documents\java\gradle-example> gradle init

Select type of project to generate:
  1: basic
  2: application
  3: library
  4: Gradle plugin
Enter selection (default: basic) [1..4] 2

Select implementation language:
  1: C++
  2: Groovy
  3: Java
  4: Kotlin
  5: Swift
Enter selection (default: Java) [1..5] 3

Select build script DSL:
  1: Groovy
  2: Kotlin
Enter selection (default: Groovy) [1..2] 1

Select test framework:
  1: JUnit 4
  2: TestNG
  3: Spock
  4: JUnit Jupiter
Enter selection (default: JUnit 4) [1..4] 1

Project name (default: gradle-example):
Source package (default: gradle.example):

> Task :init
Get more help with your project: https://docs.gradle.org/6.5.1/userguide/tutorial_java_projects.html

BUILD SUCCESSFUL in 1m 52s
2 actionable tasks: 2 executed
PS C:\Users\Administrator\Documents\java\gradle-example>
